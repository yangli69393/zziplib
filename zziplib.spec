%define disable_rpath \
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' */libtool \
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' */libtool

Name:            zziplib
Version:         0.13.69
Release:         4
Summary:         Lightweight library for zip compression
License:         LGPLv2+ or MPLv1.1
URL:             http://zziplib.sourceforge.net
Source0:         https://github.com/gdraheim/zziplib/archive/v%{version}.tar.gz

Patch6000:       CVE-2018-16548-1.patch
Patch6001:       CVE-2018-16548-2.patch
Patch6002:       CVE-2018-16548-3.patch
Patch6003:       CVE-2018-17828.patch

BuildRequires:   perl-interpreter python2 python2-rpm-macros zip xmlto
BuildRequires:   zlib-devel SDL-devel pkgconfig autoconf automake

Provides:        zziplib-utils
Obsoletes:       zziplib-utils

%description
The zziplib is a lightweight library to easily extract data from zip files. Applications
can bundle files into a single zip archive and access them. The implementation is based
only on the (free) subset of compression with the zlib algorithm which is actually used
by the zip/unzip tools.

%package         devel
Summary:         Header files and libraries for zziplib development
Requires:        %{name} = %{version}-%{release}
Requires:        pkgconfig zlib-devel SDL-devel

%description     devel
This package contains the header files and libraries needed to
develop programs that use the zziplib compression and decompression
library.

%package         help
Summary:         Help documentation for the zziplib library
Requires:        %{name} = %{version}-%{release}
BuildArch:       noarch

%description     help
This package includes help documentation and manuals related to zziplib.

%prep
%setup -q
%patch6000 -p1
%patch6001 -p1
%patch6002 -p1
%patch6003 -p1

find . -name '*.py' | xargs sed -i 's@#! /usr/bin/python@#! %__python2@g;s@#! /usr/bin/env python@#! %__python2@g'

%build
export CFLAGS="$RPM_OPT_FLAGS -fno-strict-aliasing"
export PYTHON=%__python2
%configure --disable-static --enable-sdl --enable-frame-pointer --enable-builddir=_builddir
%disable_rpath

%make_build

%install
%make_install

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%doc docs/COPYING* ChangeLog README TODO
%{_bindir}/*
%{_libdir}/*.so.*

%files devel
%doc docs/README.SDL docs/*.htm
%{_includedir}/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%{_datadir}/aclocal/*.m4
%exclude %{_libdir}/*.la

%files help
%{_mandir}/man3/*

%changelog
* Thu Jan 9 2020 BruceGW <gyl93216@163.com> - 0.13.36-4
- Delete useless patch

* Sat Dec 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 0.13.36-3
- Fix CVE-2018-16548 and CVE-2018-17828

* Thu Sep 12 2019 dongjian <dongjian13@huawei.com> 0.13.36-2
- Modification summary
